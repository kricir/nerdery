/**
 * Theme Variables
 */
var proxy = 'the-nerdery.dev';
var buildDir = 'build';
var assetsDir = 'build/assets';
var distDir = './dist';

/**
 * Load Gulp
 */
var bs = require('browser-sync').create();
var gulp = require('gulp');
var autoprefixer = require('gulp-autoprefixer');
var clean = require('gulp-clean-css');
var concat = require('gulp-concat');
// var connect = require('gulp-connect-php');
var imagemin = require('gulp-imagemin');
var sass = require('gulp-sass');
var plumber = require('gulp-plumber');
var sourcemaps = require('gulp-sourcemaps');
var uglify = require('gulp-uglify');
var util = require('gulp-util');

/**
 * Styles Task
 * Process Sass, Prefix, Clean, Minify
 */
gulp.task('styles', function() {
    return gulp.src(assetsDir + '/scss/styles.scss')
        .pipe(plumber(function(error) {
            util.log(util.colors.red('Error (' + error.plugin + '): ' + error.message));
            this.emit('end');
        }))
        .pipe(sourcemaps.init())
        .pipe(sass())
        .pipe(clean())
        .pipe(sourcemaps.write())
        .pipe(gulp.dest(distDir + '/assets/css/'))
        .pipe(bs.reload({ stream: true }));
});

/**
 * Scripts Task
 * Concat, Minify
 */
gulp.task('scripts', function() {
    return gulp.src([
            assetsDir + '/js/main.js',
        ])
        .pipe(concat('main.js'))
        .pipe(uglify())
        .pipe(gulp.dest(distDir + '/assets/js/'))
        .on("change", bs.reload);
});

/**
 * Image Processing Task
 * Minify
 */
gulp.task('images', function() {
    return gulp.src(buildDir + '/assets/images/**/*')
        .pipe(imagemin({
            progressive: true,
            interlaced: true
        }))
        .pipe(gulp.dest(distDir + '/images/'));
});

/**
 * Font Processing Task
 */
gulp.task('fonts', function() {
    return gulp.src([
            buildDir + '/assets/fonts/**/*',
            './node_modules/font-awesome/fonts/**/*'
        ])
        .pipe(gulp.dest(distDir + '/fonts/'));
});

/**
 * Raw Task
 */
gulp.task('raw', function() {
    return gulp.src(buildDir + '/**/*.{php,html}')
        .pipe(gulp.dest(distDir))
        .pipe(bs.stream());
});

/**
 * Serve Task
 */
gulp.task('serve', function() {
    bs.init({
        // proxy: proxy,
        server: {
            baseDir: distDir
        }
    })
});

/**
 * Gulp Build Tasks
 */
gulp.task('build', ['raw', 'styles', 'scripts', 'images', 'fonts']);

/**
 * Gulp Watch Command
 * Styles, Scripts, Images, Fonts
 */
gulp.task('watch', function() {
    gulp.watch(assetsDir + '/scss/**/*.scss', ['styles']);
    gulp.watch(assetsDir + '/js/**/*.js', ['scripts']);
    gulp.watch(assetsDir + '/images/**/*', ['images']);
    gulp.watch(assetsDir + '/fonts/**/*', ['fonts']);
    gulp.watch(buildDir + '/**/*.{php,html}', ['raw']);
})

/**
 * Gulp Default Tasks
 */
gulp.task('default', ['build', 'serve', 'watch']);