$(document).ready(function() {
    $('.hamburger').on('click', function() {
        $('.hamburger').toggleClass('is-active');
        activateMenu();
    });

    $('a[href^="#"]').on('click', function(event) {
        event.preventDefault();
        $('nav a').removeClass('active');
        var target = $(this.getAttribute('href'));
        $('a.nav-' + target.attr('class')).each(function() {
            $(this).addClass('active');
        });
        if ($(this).parent().attr('class') !== 'navigation') {
            $('.hamburger').toggleClass('is-active');
            activateMenu();
        }
        scrollTo(target.offset().top);
    });

    function scrollTo(offset) {
        $('html, body').stop().animate({
            scrollTop: offset
        }, 1000);
    }

    function activateMenu() {
        $('.overlay').toggleClass('is-active');
        $('.mobile-nav').toggleClass('is-active');
    }
});